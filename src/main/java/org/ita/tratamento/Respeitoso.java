/*!
 * @file Informal.java
 * @brief Implementação para formatar nomes respeitosos
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.tratamento;

/*!
 * @brief Implementação para formatação de nomes
 */
public class Respeitoso implements FormatadorNome {

    /*! Genero da pessoa */
    private Genero genero;

    /*!
     * @brief Genero/Sexo
     */
    public enum Genero {
        MASCULINO,
        FEMININO
    }

   /*!
    * @brief Atribui genero a pessoa
    * @param genero  Genero da autoridade
    */
    public Respeitoso(Genero genero) {
        this.genero = genero;
    }

    /*!
     * @brief Executa formatação some um dado nome informal
     * @param nome       Primeiro nome
     * @param sobrenome  Segundo nome
     * @return deve receber em seu construtor a informação se i
     *         é masculino ou feminino, e retornar "Sr." ou "Sra."
     *         seguido do sobrenome
     */
    public String formatarNome(String nome, String sobrenome) {
        String prefixo;

        if (genero == Genero.MASCULINO) {
            prefixo = "Sr. ";
        } else {
            prefixo = "Sra. ";
        }

        return prefixo + sobrenome;
    }
}
