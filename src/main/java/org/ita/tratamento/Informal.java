/*!
 * @file Informal.java
 * @brief Implementação para formatar nomes informais
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.tratamento;

/*!
 * @brief Implementação para formatação de nomes
 */
public class Informal implements FormatadorNome {

    /*!
     * @brief Executa formatação some um dado nome informal
     * @param nome       Primeiro nome
     * @param sobrenome  Segundo nome
     * @return retorna somente o primeiro nome
     */
    public String formatarNome(String nome, String sobrenome) {
        return nome;
    }
}
