/*!
 * @file Autoridade.java
 * @brief Implementação para autoridade de nome
 *
 *        O construstor está privado pois a classe
 *        cliente não deve depender da interface 
 *        FormatadorNome.
 *        Para encapsular este comportamento,
 *        foi gerado Builders que abstraem a 
 *        construção da interface.
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.tratamento;

import java.security.InvalidParameterException;

/*!
 * @brief Trata autoridade de nome 
 */
public class Autoridade {

    /*! Primeiro nome da autoridade */
    private String primeiroNome = "";
    /*! Segundo nomde da autoridade */
    private String segundoNome = "";
    /*! Formatador para nome da autoridade */
    private FormatadorNome formatadorNome;

    /*!
     * @brief Construtor
     * @param primeiroNome    Primeiro nome da pessoa
     * @param segundoNome     Segundo  nome da pessoa
     * @param formatadorNome  Formatador do nome
     */
    private Autoridade(String primeiroNome, String segundoNome, FormatadorNome formatadorNome) {
        this.primeiroNome = primeiroNome;
        this.segundoNome = segundoNome;
        this.formatadorNome = formatadorNome;
    }

    /*!
     * @brief Recupera o nome da pessoa com tratamento
     * @return Nome com tratamento aplicado
     */
    public String getTratamento() {
        return formatadorNome.formatarNome(primeiroNome, segundoNome);
    }

    /*!
     * @brief Instancia um novo objeto com tratamento informal
     * @param primeiroNome Primeiro nome da pessoa
     * @param segundoNome  Segunda nome da pessoa
     * @exception IllegalArgumentException se primeiro nome for vazio
     */
    public static Autoridade criarInformal(String primeiroNome, String segundoNome) {
        if (primeiroNome == null || "".equals(primeiroNome)) {
            throw new IllegalArgumentException("Primeiro nome esta vazio.");
        }
        return new Autoridade(primeiroNome, segundoNome, new Informal());
    }

    /*!
     * @brief Instancia um novo objeto com tratamento respeitoso
     * @param primeiroNome Primeiro nome da pessoa
     * @param segundoNome  Segunda nome da pessoa
     * @param genero       Genero pessoal masculino/feminino
     * @exception IllegalArgumentException se segundo nome for vazio
     */
    public static Autoridade criarRespeitoso(String primeiroNome, String segundoNome, Respeitoso.Genero genero) {
        if (segundoNome == null || "".equals(segundoNome)) {
            throw new IllegalArgumentException("Segundo nome esta vazio.");
        }
        return new Autoridade(primeiroNome, segundoNome, new Respeitoso(genero));
    }

    /*!
     * @brief Instancia um novo objeto com tratamento titulado
     * @param primeiroNome Primeiro nome da pessoa
     * @param segundoNome  Segunda nome da pessoa
     * @exception IllegalArgumentException se primeiro nome for vazio
     * @exception IllegalArgumentException se segundo nome for vazio
     * @exception IllegalArgumentException se titulo nome for vazio
     */
    public static Autoridade criarComTitulo(String primeiroNome, String segundoNome, String titulo) {
        if (primeiroNome == null || "".equals(primeiroNome)) {
            throw new IllegalArgumentException("Primeiro nome esta vazio.");
        }
        if (segundoNome == null || "".equals(segundoNome)) {
            throw new IllegalArgumentException("Segundo nome esta vazio.");
        }
        if (titulo == null || "".equals(titulo)) {
          throw new IllegalArgumentException("titulo esta vazio.");
        }
        return new Autoridade(primeiroNome, segundoNome, new ComTitulo(titulo));
    }

}
