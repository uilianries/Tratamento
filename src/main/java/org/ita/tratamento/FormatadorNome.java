/*!
 * @file FormatadorNome.java
 * @brief Interface para formatar nomes
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.tratamento;

/*!
 * @brief Contrato para formatação de nomes
 */
public interface FormatadorNome {

    /*!
     * @brief Executa formatação some um dado nome
     * @param nome       Primeiro nome
     * @param sobrenome  Segundo nome
     * @return Nome formatado
     */
    public String formatarNome(String nome, String sobrenome);
}
