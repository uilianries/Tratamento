/*!
 * @file ComTitulo.java
 * @brief Implementação para formatar nomes com titulo
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.tratamento;

/*!
 * @brief Implementação para formatação de nomes
 */
public class ComTitulo implements FormatadorNome {
    /*! Titulo da autoridade */
    private String titulo;

    /*!
     * @brief Atribui nome titulo
     * @param titulo   Titulo da autoridade
     */
    public ComTitulo(String titulo) {
        this.titulo = titulo;
    }

    /*!
     * @brief Executa formatação some um dado nome informal
     * @param nome       Primeiro nome
     * @param sobrenome  Segundo nome
     * @return retorna o título seguido de nome e sobrenome
     */
    public String formatarNome(String nome, String sobrenome) {
        return titulo + " " + nome + " " + sobrenome;
    }
}
