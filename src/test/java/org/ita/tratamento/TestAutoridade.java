/*!
 * @file TestAutoridade.java
 * @brief Valida cada caso de autodidade
 *
 * @author Uilian Ries <uilian.ries@gmai.com>
 */
package org.ita.tratamento;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.lang.IllegalArgumentException;

/*!
 * @brief Validação autoridades
 */
public class TestAutoridade {

    /*! Primeiro nome masculino */
    private static final String nomeMasculino = "James";
    /*! Primeiro nome feminino */
    private static final String nomeFeminino = "Joane";
    /*! Segundo nome */
    private static final String segundoNome = "Gosling";
    /*! Titulo de Doutorado */
    private static final String titulo = "Dr.";
    /*! Instancia compartilhada */
    private Autoridade autoridade = null;

    /*!
     * @brief Valida autoridade informal
     */
    @Test
    public void testInformal() {
        autoridade = Autoridade.criarInformal(nomeMasculino, segundoNome);
        assertEquals(nomeMasculino, autoridade.getTratamento());
    }

    /*!
     * @brief Valida autoridade informal com nome nulo
     */
    @Test(expected=IllegalArgumentException.class)
    public void testInformalNomeNulo() {
        autoridade = Autoridade.criarInformal("", segundoNome);
    }

    /*!
     * @brief Valida autoridade informal com sobrenome nulo
     */
    @Test
    public void testInformalSobrenomeNulo() {
        autoridade = Autoridade.criarInformal(nomeMasculino, "");
        assertEquals(nomeMasculino, autoridade.getTratamento());
    }

    /*!
     * @brief Valida autoridade respeitosa masculina
     */
    @Test
    public void testRespeitosoMasculino() {
        autoridade = Autoridade.criarRespeitoso(nomeMasculino, segundoNome, Respeitoso.Genero.MASCULINO);
        final String esperado = "Sr. " + segundoNome;
        assertEquals(esperado, autoridade.getTratamento());
    }

    /*!
     * @brief Valida autoridade respeitosa feminina
     */
    @Test
    public void testRespeitosoFeminino() {
        autoridade = Autoridade.criarRespeitoso(nomeFeminino, segundoNome, Respeitoso.Genero.FEMININO);
        final String esperado = "Sra. " + segundoNome;
        assertEquals(esperado, autoridade.getTratamento());
    }

    /*!
     * @brief Valida autoridade respeitosa nula
     */
    @Test(expected=IllegalArgumentException.class)
    public void testRespeitosoNulo() {
        autoridade = Autoridade.criarRespeitoso(nomeMasculino, "", Respeitoso.Genero.MASCULINO);
    }

    /*!
     * @brief Valida autoridade com titulo
     */
    @Test
    public void testEntitulado() {
        autoridade = Autoridade.criarComTitulo(nomeMasculino, segundoNome, titulo);
        final String esperado = titulo + " " + nomeMasculino + " " + segundoNome;
        assertEquals(esperado, autoridade.getTratamento());
    }

    /*!
     * @brief Valida autoridade com titulo e nome nulo
     */
    @Test(expected=IllegalArgumentException.class)
    public void testEntituladoNomeNulo() {
        autoridade = Autoridade.criarComTitulo("", segundoNome, titulo);
    }

    /*!
     * @brief Valida autoridade com titulo e sobrenome nulo
     */
    @Test(expected=IllegalArgumentException.class)
    public void testEntituladoSobreNulo() {
        autoridade = Autoridade.criarComTitulo(nomeMasculino, "", titulo);
    }

    /*!
     * @brief Valida autoridade com titulo nulo
     */
    @Test(expected=IllegalArgumentException.class)
    public void testEntituladoNulo() {
        autoridade = Autoridade.criarComTitulo(nomeMasculino, segundoNome, "");
    }
}
